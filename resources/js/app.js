require('./bootstrap');

window.Vue = require('vue');

//Primi apici, mettete il nome del tag html. Si chiamerà michele in questo caso.
//Il secondo è il nome del file all'interno della cartella components
// Vue.component('michele', require('./components/Michele.vue').default);
Vue.component('esercizio-dischi', require('./components/EsercizioDischi.vue').default);
Vue.component('b-header', require('./components/BHeader.vue').default);
Vue.component('b-cds-container', require('./components/BCdsContainer.vue').default);
Vue.component('b-cd', require('./components/BCd.vue').default);
Vue.component('b-footer', require('./components/BFooter.vue').default);

const app = new Vue({
    el: '#app',
    data: {
    }
});
